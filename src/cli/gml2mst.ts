import { readFileSync, writeFileSync } from "fs";
import path from "path";
import { getErrorInfo, getErrors, lex, parse } from "gml";
import { render } from "../library";
import program from "commander";

program
    .version("1.0.0")
    .option("-o --output <path>", "Output destination")
    .option("-i --input <path>", "Input destination")
    .parse(process.argv);

if (!program.input) {
    console.error("ERROR: Must specify an input file");
    process.exitCode = 1;
} else if (!program.output) {
    console.error("ERROR: Must specify an output file");
    process.exitCode = 1;
} else {
    const input = readFileSync(path.resolve(program.input)).toString();
    const output = path.resolve(program.output);

    const tokens = lex(input);
    if (getErrors().length > 0) {
        throw new Error(
            "Could not lex file:\n" +
                JSON.stringify(getErrorInfo(getErrors(), input), null, 2)
        );
    }
    const ast = parse(tokens);
    if (getErrors().length > 0) {
        throw new Error(
            "Could not parse file:\n" +
                JSON.stringify(getErrorInfo(getErrors(), input), null, 2)
        );
    }

    writeFileSync(output, render(ast));
}