'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var gml = require('gml');
var crypto = _interopDefault(require('crypto'));

// Unique ID creation requires a high quality random # generator.  In node.js
// this is pretty straight-forward - we use the crypto API.



var rng = function nodeRNG() {
  return crypto.randomBytes(16);
};

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];
for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex;
  // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
  return ([bth[buf[i++]], bth[buf[i++]], 
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]]]).join('');
}

var bytesToUuid_1 = bytesToUuid;

// **`v1()` - Generate time-based UUID**
//
// Inspired by https://github.com/LiosK/UUID.js
// and http://docs.python.org/library/uuid.html

var _nodeId;
var _clockseq;

// Previous uuid creation time
var _lastMSecs = 0;
var _lastNSecs = 0;

// See https://github.com/broofa/node-uuid for API details
function v1(options, buf, offset) {
  var i = buf && offset || 0;
  var b = buf || [];

  options = options || {};
  var node = options.node || _nodeId;
  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

  // node and clockseq need to be initialized to random values if they're not
  // specified.  We do this lazily to minimize issues related to insufficient
  // system entropy.  See #189
  if (node == null || clockseq == null) {
    var seedBytes = rng();
    if (node == null) {
      // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
      node = _nodeId = [
        seedBytes[0] | 0x01,
        seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]
      ];
    }
    if (clockseq == null) {
      // Per 4.2.2, randomize (14 bit) clockseq
      clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
    }
  }

  // UUID timestamps are 100 nano-second units since the Gregorian epoch,
  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

  // Per 4.2.1.2, use count of uuid's generated during the current clock
  // cycle to simulate higher resolution clock
  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

  // Time since last uuid creation (in msecs)
  var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

  // Per 4.2.1.2, Bump clockseq on clock regression
  if (dt < 0 && options.clockseq === undefined) {
    clockseq = clockseq + 1 & 0x3fff;
  }

  // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
  // time interval
  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
    nsecs = 0;
  }

  // Per 4.2.1.2 Throw error if too many uuids are requested
  if (nsecs >= 10000) {
    throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
  }

  _lastMSecs = msecs;
  _lastNSecs = nsecs;
  _clockseq = clockseq;

  // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
  msecs += 12219292800000;

  // `time_low`
  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
  b[i++] = tl >>> 24 & 0xff;
  b[i++] = tl >>> 16 & 0xff;
  b[i++] = tl >>> 8 & 0xff;
  b[i++] = tl & 0xff;

  // `time_mid`
  var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
  b[i++] = tmh >>> 8 & 0xff;
  b[i++] = tmh & 0xff;

  // `time_high_and_version`
  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
  b[i++] = tmh >>> 16 & 0xff;

  // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
  b[i++] = clockseq >>> 8 | 0x80;

  // `clock_seq_low`
  b[i++] = clockseq & 0xff;

  // `node`
  for (var n = 0; n < 6; ++n) {
    b[i + n] = node[n];
  }

  return buf ? buf : bytesToUuid_1(b);
}

var v1_1 = v1;

function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof(options) == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }
  options = options || {};

  var rnds = options.random || (options.rng || rng)();

  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
  rnds[6] = (rnds[6] & 0x0f) | 0x40;
  rnds[8] = (rnds[8] & 0x3f) | 0x80;

  // Copy bytes to buffer, if provided
  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || bytesToUuid_1(rnds);
}

var v4_1 = v4;

var uuid = v4_1;
uuid.v1 = v1_1;
uuid.v4 = v4_1;

var uuid_1 = uuid;

var headers = "import { types, SnapshotIn, SnapshotOut, Instance } from \"mobx-state-tree\";";
var helperFunctions = "\nconst isOfLength = (l: number) => (snap: { length: number } | undefined) => {\n    if (snap === undefined) return false;\n    return snap.length === l;\n};\n\nconst isInRange = (low: number, high:number) => (snap: { length: number } | undefined) => {\n    if (snap === undefined) return false;\n    const l = snap.length;\n    return l >= low && l <= high;\n};\n";
function render(file) {
    return "/*\nTHIS FILE WAS GENERATED BY A SCRIPT.\nDO NOT MODIFY IT DIRECTLY.\n*/    \n" + headers + "\n\n// HELPER FUNCTIONS\n" + helperFunctions + "\n\n// DATA\n" + renderFile(file) + "\n\n\n// PLACEHOLDERS\n" + renderPlaceholders(placeholders) + "\n";
}
// Maps the values to declaration names
var valToDeclName = {};
var placeholders = {};
var p = "";
function renderDefaultJSON(file) {
    renderFile(file);
    return placeholders;
}
function renderPlaceholders(placeholders) {
    var strings = Object.keys(placeholders).map(function (k) {
        var p = placeholders[k];
        var n = k.split("Model")[0];
        return "export function createDefault" + n + "():" + n + "SnapOut {return " + p + "}";
    });
    return strings.join("\n");
}
function renderFile(file) {
    placeholders = {};
    var declarationStrings = [];
    file.declarations.forEach(function (d) { return declarationStrings.push(renderDecl(d)); });
    return declarationStrings.join("\n");
}
function renderDecl(decl) {
    var baseName = decl.identifier.str;
    var name = baseName;
    if (gml.Obj.is(decl.value)) {
        name = name + "Model";
        p = "";
    }
    var value = renderValue(decl.value);
    var out;
    if (gml.Obj.is(decl.value)) {
        out = "export const " + name + " = " + value + ".named(\"" + name + "\");\nexport type " + baseName + "SnapIn = SnapshotIn<typeof " + name + ">;\nexport type " + baseName + "SnapOut = SnapshotOut<typeof " + name + ">;\nexport type " + baseName + " = Instance<typeof " + name + ">;\n";
        placeholders[name] = p;
    }
    else {
        out = "export const " + name + " = " + value + ";";
    }
    valToDeclName[value] = name;
    if (decl.comment)
        out = "\n// " + decl.comment + "\n" + out;
    return out;
}
function renderField(field) {
    var comment = "" + (field.comment ? "// " + field.comment + "\n" : "");
    var name = "" + field.identifier.str;
    if (field.isIdentifier) {
        p += " " + name + ": \"" + uuid_1.v4() + "\",";
        return "" + comment + name + ": types.identifier,";
    }
    else {
        p += " " + name + ":";
        var val = renderValue(field.value);
        p += ",";
        return "" + comment + name + ": " + val + ",";
    }
}
function renderValue(node) {
    var result;
    switch (node.type) {
        // Literals
        case gml.NodeType.NumLiteral:
            result = renderNumLiteral(node);
            break;
        case gml.NodeType.StrLiteral:
            result = renderStrLiteral(node);
            break;
        case gml.NodeType.BoolLiteral:
            result = renderBoolLiteral(node);
            break;
        // Types
        case gml.NodeType.Number:
        case gml.NodeType.String:
        case gml.NodeType.Bool:
        case gml.NodeType.Url:
        case gml.NodeType.Image:
        case gml.NodeType.Video:
        case gml.NodeType.Sound:
        case gml.NodeType.Range:
        case gml.NodeType.Reference:
        case gml.NodeType.Object:
        case gml.NodeType.List:
        case gml.NodeType.Set:
        case gml.NodeType.Map:
            return renderType(node);
        default:
            throw new Error("Invalid value type: " + node.type);
    }
    var cached = valToDeclName[result];
    if (cached !== undefined)
        return cached;
    return result;
}
function renderNumLiteral(node) {
    if (!gml.NumLiteral.is(node))
        throw new Error("Expected a num literal but got a: " + node.type);
    p += node.value;
    return "" + node.value;
}
function renderStrLiteral(node) {
    if (!gml.StrLiteral.is(node))
        throw new Error("Expected a str literal but got a: " + node.type);
    p += "\"" + node.value + "\"";
    return "" + node.value;
}
function renderBoolLiteral(node) {
    if (!gml.BoolLiteral.is(node))
        throw new Error("Expected a bool literal but got a: " + node.type);
    p += node.value;
    return "" + node.value;
}
function renderType(node) {
    var result;
    switch (node.type) {
        case gml.NodeType.Number:
            result = renderNumber(node);
            break;
        case gml.NodeType.String:
            result = renderString(node);
            break;
        case gml.NodeType.Bool:
            result = renderBool(node);
            break;
        case gml.NodeType.Url:
            result = renderUrl(node);
            break;
        case gml.NodeType.Image:
            result = renderImage(node);
            break;
        case gml.NodeType.Video:
            result = renderVideo(node);
            break;
        case gml.NodeType.Sound:
            result = renderSound(node);
            break;
        case gml.NodeType.Range:
            result = renderRange(node);
            break;
        case gml.NodeType.Reference:
            result = renderReference(node);
            break;
        case gml.NodeType.Object:
            result = renderObj(node);
            break;
        case gml.NodeType.List:
            result = renderList(node);
            break;
        case gml.NodeType.Set:
            result = renderSet(node);
            break;
        case gml.NodeType.Map:
            result = renderMap(node);
            break;
        default:
            throw new Error("Invalid type type: " + node.type);
    }
    var cached = valToDeclName[result];
    if (cached !== undefined)
        return cached;
    return result;
}
function renderNumber(node) {
    if (!gml.Num.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += node.defaultValue;
        return "" + node.defaultValue;
    }
    else {
        p += -1;
        return "types.number";
    }
}
function renderBool(node) {
    if (!gml.Bool.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += node.defaultValue;
        return "" + node.defaultValue;
    }
    else {
        p += "false";
        return "types.boolean";
    }
}
function renderString(node) {
    if (!gml.Str.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this string\"";
        return "types.string";
    }
}
function renderUrl(node) {
    if (!gml.Url.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this url\"";
        return "types.string";
    }
}
function renderImage(node) {
    if (!gml.Image.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this image url\"";
        return "types.string";
    }
}
function renderVideo(node) {
    if (!gml.Video.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this video url\"";
        return "types.string";
    }
}
function renderSound(node) {
    if (!gml.Sound.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this sound url\"";
        return "types.string";
    }
}
function renderRange(node) {
    if (!gml.Range.is(node))
        throw new Error("Unexpected node type");
    p += node.lowerBound;
    return "types.refinement(types.number, isInRange(" + node.lowerBound + "," + node.upperBound + "))";
}
function renderReference(node) {
    if (!gml.Ref.is(node))
        throw new Error("Unexpected node type");
    p += "\"" + node.identifierName + "\"";
    return "types.reference(" + node.identifierName + "Model)";
}
function renderSet(node) {
    if (!gml.GMLSet.is(node))
        throw new Error("Unexpected node type");
    var values = node.values;
    var valueStrings;
    var val = node.values[0];
    if (typeof val === "string") {
        p += "\"" + val + "\"";
        valueStrings = values.map(function (v) { return "types.literal(\"" + v + "\")"; });
    }
    else if (typeof values[0] === "number") {
        p += val;
        valueStrings = values.map(function (v) { return "types.literal(" + v + ")"; });
    }
    else
        throw new Error("Unexpected set type");
    return "types.union(" + valueStrings.join(",") + ")";
}
function renderMap(node) {
    if (!gml.GMLMap.is(node))
        throw new Error("Unexpected node type");
    var name = "Key";
    if (gml.Ref.is(node.keyType)) {
        name = node.keyType.identifierName;
    }
    else if (gml.Str.is(node.keyType)) {
        name = node.keyType.defaultValue || "Key";
    }
    else
        throw new Error("Unexpected map key type");
    p += "{" + name + ":";
    var type = renderType(node.valueType);
    p += "}";
    return "types.map(" + type + ")";
}
function renderObj(obj) {
    if (!gml.Obj.is(obj))
        throw new Error("Unexpected type");
    var fieldsStrings = [];
    p += "{";
    obj.fields.forEach(function (f) { return fieldsStrings.push(renderField(f)); });
    var fields = fieldsStrings.join("\n");
    p += "}";
    return "types.model({\n" + fields + "\n})";
}
function renderList(list) {
    if (!gml.List.is(list))
        throw new Error("Unexpected type");
    var out;
    p += "[";
    var previous = p;
    p = "";
    var array = "types.array(" + renderType(list.typeParam) + ")";
    var element = p;
    var size;
    if (gml.Range.is(list.size)) {
        size = list.size.lowerBound;
        out = "types.refinement(" + array + ", isInRange(" + list.size.lowerBound + ", " + list.size.upperBound + "))";
    }
    else if (typeof list.size === "number") {
        var value = "" + list.size;
        var cached = valToDeclName[value];
        if (cached !== undefined)
            value = cached;
        size = list.size;
        out = "types.refinement(" + array + ", isOfLength(" + value + "))";
    }
    else {
        size = 1;
        out = array;
    }
    p = previous;
    var elements = [];
    for (var i = 0; i < size; i++)
        elements.push(element);
    p += elements.join(",");
    p += "]";
    return out;
}

exports.render = render;
exports.renderDefaultJSON = renderDefaultJSON;
