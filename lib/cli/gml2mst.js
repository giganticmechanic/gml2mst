'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var fs = require('fs');
var fs__default = _interopDefault(fs);
var path = _interopDefault(require('path'));
var crypto = _interopDefault(require('crypto'));
var events = _interopDefault(require('events'));
var child_process = _interopDefault(require('child_process'));
var util = _interopDefault(require('util'));

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var TokenType;
(function (TokenType) {
    TokenType["OPEN_PAREN"] = "Open Paren";
    TokenType["CLOSED_PAREN"] = "Closed Paren";
    TokenType["OPEN_BRACE"] = "Open Brace";
    TokenType["CLOSED_BRACE"] = "Closed Brace";
    TokenType["OPEN_BRACKET"] = "Open Bracket";
    TokenType["CLOSED_BRACKET"] = "Closed Bracket";
    TokenType["OPEN_ANGLE_BRACKET"] = "Open Angle Bracket";
    TokenType["CLOSED_ANGLE_BRACKET"] = "Closed Angle Bracket";
    TokenType["STAR"] = "Star";
    TokenType["COMMA"] = "Comma";
    TokenType["EQUALS"] = "Equals";
    TokenType["COLON"] = "Colon";
    TokenType["STR"] = "String";
    TokenType["NUM"] = "Number";
    TokenType["BOOL"] = "Bool";
    TokenType["IDENTIFIER"] = "Identifier";
    TokenType["COMMENT"] = "Comment";
    TokenType["EOF"] = "EOF";
})(TokenType || (TokenType = {}));
var isTokenType = function (tokenType) { return function (token) {
    return token.type === tokenType;
}; };
var isEOF = isTokenType(TokenType.EOF);
var isIdentifier = isTokenType(TokenType.IDENTIFIER);
var isEquals = isTokenType(TokenType.EQUALS);
var isColon = isTokenType(TokenType.COLON);
var isStar = isTokenType(TokenType.STAR);
var isComma = isTokenType(TokenType.COMMA);
var isComment = isTokenType(TokenType.COMMENT);
var isNumLiteral = isTokenType(TokenType.NUM);
var isStrLiteral = isTokenType(TokenType.STR);
var isBoolLiteral = isTokenType(TokenType.BOOL);
var isOpenBrace = isTokenType(TokenType.OPEN_BRACE);
var isClosedBrace = isTokenType(TokenType.CLOSED_BRACE);
var isOpenAngleBracket = isTokenType(TokenType.OPEN_ANGLE_BRACKET);
var isClosedAngleBracket = isTokenType(TokenType.CLOSED_ANGLE_BRACKET);
var isOpenBracket = isTokenType(TokenType.OPEN_BRACKET);
var isClosedBracket = isTokenType(TokenType.CLOSED_BRACKET);
var isOpenParen = isTokenType(TokenType.OPEN_PAREN);
var isClosedParen = isTokenType(TokenType.CLOSED_PAREN);

var NodeType;
(function (NodeType) {
    NodeType["File"] = "File";
    NodeType["Declaration"] = "Declaration";
    NodeType["Field"] = "Field";
    NodeType["Object"] = "Object";
    NodeType["List"] = "List";
    NodeType["NumLiteral"] = "Numeric Literal";
    NodeType["StrLiteral"] = "String Literal";
    NodeType["BoolLiteral"] = "Boolean Literal";
    NodeType["Number"] = "Number";
    NodeType["String"] = "String";
    NodeType["Bool"] = "Bool";
    NodeType["Url"] = "Url";
    NodeType["Image"] = "Image";
    NodeType["Video"] = "Video";
    NodeType["Sound"] = "Sound";
    NodeType["Reference"] = "Reference";
    NodeType["Range"] = "Range";
    NodeType["Set"] = "Set";
    NodeType["Map"] = "Map";
})(NodeType || (NodeType = {}));
function isPrimitive(n) {
    return (Num.is(n) ||
        Str.is(n) ||
        Bool.is(n) ||
        Url.is(n) ||
        Video.is(n) ||
        Image.is(n) ||
        Sound.is(n));
}
function isType(n) {
    return (isPrimitive(n) ||
        Obj.is(n) ||
        List.is(n) ||
        Ref.is(n) ||
        Range.is(n) ||
        GMLSet.is(n) ||
        GMLMap.is(n));
}
var classToNode = new Map();
function registerType(type, ctr) {
    classToNode.set(ctr, type);
}
var ASTNode = (function () {
    function ASTNode(type, children) {
        this.type = type;
        this.children = children;
    }
    return ASTNode;
}());
var File = (function (_super) {
    __extends(File, _super);
    function File(declarations) {
        var _this = _super.call(this, NodeType.File, declarations) || this;
        _this.declarations = declarations;
        return _this;
    }
    return File;
}(ASTNode));
registerType(NodeType.File, File);
var Declaration = (function (_super) {
    __extends(Declaration, _super);
    function Declaration(identifier, value, comment) {
        var _this = _super.call(this, NodeType.Declaration, [value]) || this;
        _this.identifier = identifier;
        _this.value = value;
        _this.comment = comment;
        if (identifier.type !== TokenType.IDENTIFIER) {
            throw Error("Expected an identifier but got: " + identifier.type);
        }
        return _this;
    }
    Declaration.is = function (arg) {
        return arg instanceof Declaration;
    };
    return Declaration;
}(ASTNode));
registerType(NodeType.Declaration, Declaration);
var Obj = (function (_super) {
    __extends(Obj, _super);
    function Obj(fields) {
        var _this = _super.call(this, NodeType.Object, fields) || this;
        _this.fields = fields;
        return _this;
    }
    Obj.is = function (arg) {
        return arg instanceof Obj;
    };
    return Obj;
}(ASTNode));
registerType(NodeType.Object, Obj);
var Field = (function (_super) {
    __extends(Field, _super);
    function Field(identifier, value, comment, isIdentifier) {
        var _this = _super.call(this, NodeType.Field, [value]) || this;
        _this.identifier = identifier;
        _this.value = value;
        _this.comment = comment;
        _this.isIdentifier = isIdentifier;
        if (identifier.type !== TokenType.IDENTIFIER) {
            throw Error("Expected an identifier but got: " + identifier.type);
        }
        return _this;
    }
    Field.is = function (arg) {
        return arg instanceof Field;
    };
    return Field;
}(ASTNode));
registerType(NodeType.Field, Field);
var List = (function (_super) {
    __extends(List, _super);
    function List(typeParam, size) {
        var _this = _super.call(this, NodeType.List, [typeParam]) || this;
        _this.typeParam = typeParam;
        _this.size = size;
        return _this;
    }
    List.is = function (arg) {
        return arg instanceof List;
    };
    return List;
}(ASTNode));
registerType(NodeType.List, List);
var LiteralValue = (function (_super) {
    __extends(LiteralValue, _super);
    function LiteralValue(value, type) {
        var _this = _super.call(this, type, []) || this;
        _this.value = value;
        return _this;
    }
    return LiteralValue;
}(ASTNode));
var NumLiteral = (function (_super) {
    __extends(NumLiteral, _super);
    function NumLiteral(value) {
        return _super.call(this, value, NodeType.NumLiteral) || this;
    }
    NumLiteral.is = function (arg) {
        return arg instanceof NumLiteral;
    };
    return NumLiteral;
}(LiteralValue));
registerType(NodeType.NumLiteral, NumLiteral);
var StrLiteral = (function (_super) {
    __extends(StrLiteral, _super);
    function StrLiteral(value) {
        return _super.call(this, value, NodeType.StrLiteral) || this;
    }
    StrLiteral.is = function (arg) {
        return arg instanceof StrLiteral;
    };
    return StrLiteral;
}(LiteralValue));
registerType(NodeType.StrLiteral, StrLiteral);
var BoolLiteral = (function (_super) {
    __extends(BoolLiteral, _super);
    function BoolLiteral(value) {
        return _super.call(this, value, NodeType.BoolLiteral) || this;
    }
    BoolLiteral.is = function (arg) {
        return arg instanceof BoolLiteral;
    };
    return BoolLiteral;
}(LiteralValue));
registerType(NodeType.BoolLiteral, BoolLiteral);
var PrimitiveDecl = (function (_super) {
    __extends(PrimitiveDecl, _super);
    function PrimitiveDecl(defaultValue, type) {
        var _this = _super.call(this, type, []) || this;
        _this.defaultValue = defaultValue;
        return _this;
    }
    return PrimitiveDecl;
}(ASTNode));
var Num = (function (_super) {
    __extends(Num, _super);
    function Num(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Number) || this;
    }
    Num.is = function (arg) {
        return arg instanceof Num;
    };
    return Num;
}(PrimitiveDecl));
registerType(NodeType.Number, Num);
var Str = (function (_super) {
    __extends(Str, _super);
    function Str(defaultValue) {
        return _super.call(this, defaultValue, NodeType.String) || this;
    }
    Str.is = function (arg) {
        return arg instanceof Str;
    };
    return Str;
}(PrimitiveDecl));
registerType(NodeType.String, Str);
var Bool = (function (_super) {
    __extends(Bool, _super);
    function Bool(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Bool) || this;
    }
    Bool.is = function (arg) {
        return arg instanceof Bool;
    };
    return Bool;
}(PrimitiveDecl));
registerType(NodeType.Bool, Bool);
var Url = (function (_super) {
    __extends(Url, _super);
    function Url(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Url) || this;
    }
    Url.is = function (arg) {
        return arg instanceof Url;
    };
    return Url;
}(PrimitiveDecl));
registerType(NodeType.Url, Url);
var Image = (function (_super) {
    __extends(Image, _super);
    function Image(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Image) || this;
    }
    Image.is = function (arg) {
        return arg instanceof Image;
    };
    return Image;
}(PrimitiveDecl));
registerType(NodeType.Image, Image);
var Video = (function (_super) {
    __extends(Video, _super);
    function Video(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Video) || this;
    }
    Video.is = function (arg) {
        return arg instanceof Video;
    };
    return Video;
}(PrimitiveDecl));
registerType(NodeType.Video, Video);
var Sound = (function (_super) {
    __extends(Sound, _super);
    function Sound(defaultValue) {
        return _super.call(this, defaultValue, NodeType.Sound) || this;
    }
    Sound.is = function (arg) {
        return arg instanceof Sound;
    };
    return Sound;
}(PrimitiveDecl));
registerType(NodeType.Sound, Sound);
var Ref = (function (_super) {
    __extends(Ref, _super);
    function Ref(identifierName) {
        var _this = _super.call(this, NodeType.Reference, []) || this;
        _this.identifierName = identifierName;
        return _this;
    }
    Ref.is = function (arg) {
        return arg instanceof Ref;
    };
    return Ref;
}(ASTNode));
registerType(NodeType.Reference, Ref);
var Range = (function (_super) {
    __extends(Range, _super);
    function Range(lowerBound, upperBound) {
        var _this = _super.call(this, NodeType.Range, []) || this;
        _this.lowerBound = lowerBound;
        _this.upperBound = upperBound;
        return _this;
    }
    Range.is = function (arg) {
        return arg instanceof Range;
    };
    return Range;
}(ASTNode));
registerType(NodeType.Range, Range);
var GMLSet = (function (_super) {
    __extends(GMLSet, _super);
    function GMLSet(values) {
        var _this = _super.call(this, NodeType.Set, []) || this;
        _this.values = values;
        return _this;
    }
    GMLSet.is = function (arg) {
        return arg instanceof GMLSet;
    };
    return GMLSet;
}(ASTNode));
registerType(NodeType.Set, GMLSet);
var GMLMap = (function (_super) {
    __extends(GMLMap, _super);
    function GMLMap(keyType, valueType) {
        var _this = _super.call(this, NodeType.Map, []) || this;
        _this.keyType = keyType;
        _this.valueType = valueType;
        return _this;
    }
    GMLMap.is = function (arg) {
        return arg instanceof GMLMap;
    };
    return GMLMap;
}(ASTNode));
registerType(NodeType.Map, GMLMap);

var errors = [];
var getErrors = function () { return errors.slice(0); };
function pushError(err) {
    errors.push(err);
}
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["InvalidToken"] = 0] = "InvalidToken";
    ErrorType[ErrorType["UnterminatedString"] = 1] = "UnterminatedString";
    ErrorType[ErrorType["UnterminatedComment"] = 2] = "UnterminatedComment";
    ErrorType[ErrorType["ParseError"] = 3] = "ParseError";
})(ErrorType || (ErrorType = {}));
var GMLError = (function () {
    function GMLError(type, info, description, offset) {
        this.type = type;
        this.info = info;
        this.description = description;
        this.offset = offset;
    }
    return GMLError;
}());
var InvalidTokenError = (function (_super) {
    __extends(InvalidTokenError, _super);
    function InvalidTokenError(message, offset) {
        return _super.call(this, ErrorType.InvalidToken, message, InvalidTokenError.description, offset) || this;
    }
    InvalidTokenError.is = function (arg) {
        return arg.constructor === InvalidTokenError;
    };
    InvalidTokenError.description = "Invalid Token";
    return InvalidTokenError;
}(GMLError));
var UnterminatedStringError = (function (_super) {
    __extends(UnterminatedStringError, _super);
    function UnterminatedStringError(offset) {
        return _super.call(this, ErrorType.UnterminatedString, "All strings need a closing quote (\")", UnterminatedStringError.description, offset) || this;
    }
    UnterminatedStringError.is = function (arg) {
        return arg.constructor === UnterminatedStringError;
    };
    UnterminatedStringError.description = "No closing quote";
    return UnterminatedStringError;
}(GMLError));
var UnterminatedCommentError = (function (_super) {
    __extends(UnterminatedCommentError, _super);
    function UnterminatedCommentError(offset) {
        return _super.call(this, ErrorType.UnterminatedComment, "All comments need a closing >>", UnterminatedCommentError.description, offset) || this;
    }
    UnterminatedCommentError.is = function (arg) {
        return arg.constructor === UnterminatedCommentError;
    };
    UnterminatedCommentError.description = "No closing >>";
    return UnterminatedCommentError;
}(GMLError));
var ParseError = (function (_super) {
    __extends(ParseError, _super);
    function ParseError(message, token) {
        return _super.call(this, ErrorType.ParseError, message, ParseError.description, token.offset) || this;
    }
    ParseError.is = function (arg) {
        return arg.constructor === ParseError;
    };
    ParseError.description = "Unparseable token";
    return ParseError;
}(GMLError));
function getErrorInfo(errors, input) {
    var lines = input.split("\n");
    var getLocation = function (offset) {
        var o = offset;
        if (o !== -1) {
            var c = -1;
            for (var i = 0; i < lines.length; i++) {
                var len = lines[i].length;
                c += len + 1;
                if (o <= c) {
                    return {
                        line: i + 1,
                        column: o - (c - len)
                    };
                }
            }
        }
        return { line: -1, column: -1 };
    };
    var createInfo = function (err) {
        var _a = getLocation(err.offset), line = _a.line, column = _a.column;
        if (line === -1) {
            return {
                message: err.description + ": " + err.info,
                context: null,
                line: -1,
                column: -1
            };
        }
        else {
            var errorLine = lines[line - 1];
            return {
                context: errorLine,
                message: "[" + err.description + "] " + err.info,
                line: line,
                column: column
            };
        }
    };
    return errors.map(createInfo);
}

function printToken(token) {
    return "[" + token.type + "]:" + token.str;
}
function getNumValue(token) {
    if (token.type !== TokenType.NUM) {
        throw Error("Cannot get number value from non-number token");
    }
    var val = parseFloat(token.str);
    if (val === undefined)
        throw Error("Could not parse " + token.str + " as a number");
    return val;
}
function getBoolValue(token) {
    if (token.type !== TokenType.BOOL) {
        throw Error("Cannot get bool value from non-bool token");
    }
    switch (token.str) {
        case "true":
            return true;
        case "false":
            return false;
        default:
            throw Error("Bool token has invalid string: " + token.str);
    }
}

function createStream(data) {
    return { data: data, current: 0 };
}
var isFinished = function (stream) {
    return stream.current >= stream.data.length;
};
var advance = function (stream) {
    if (isFinished(stream))
        throw "Tried to advance past end of stream.";
    return stream.data[stream.current++];
};
var peek = function (stream, offset) {
    if (offset === void 0) { offset = 0; }
    var d = stream.data[stream.current + offset];
    if (d === undefined)
        throw Error("Peeked out of bounds");
    return d;
};
var test = function (stream, predicate) {
    if (isFinished(stream))
        return false;
    var v = peek(stream);
    if (v === undefined)
        return false;
    return predicate(v);
};
var testAhead = function (stream, offset, predicate) {
    if (stream.current + offset >= stream.data.length)
        return false;
    var v = peek(stream, offset);
    if (v === undefined)
        return false;
    return predicate(v);
};
var advanceIf = function (stream, predicate, errHandler) {
    if (isFinished(stream))
        throw Error("Unexpectedly reached end of stream.");
    if (!test(stream, predicate)) {
        var t = peek(stream);
        if (errHandler !== undefined)
            errHandler(t);
        return t;
    }
    return advance(stream);
};

var Reserved = {
    True: "true",
    False: "false",
    Bool: "bool",
    Number: "number",
    String: "string",
    Url: "url",
    Video: "video",
    Image: "image",
    Sound: "sound",
    Ref: "ref",
    List: "list",
    Map: "map",
    Set: "set"
};
function invert(o) {
    var result = {};
    Object.keys(o).forEach(function (key) {
        var val = o[key];
        result[val] = key;
    });
    return result;
}
var ReservedByValue = invert(Reserved);
var isReserved = function (str) {
    return ReservedByValue[str] !== undefined;
};

var error = function (message, token) {
    throw new ParseError(message, token);
};
var expected = function (expectedString) { return function (t) {
    var errMsg = "Expected: ( " + expectedString + " ) Received: ( " + t.str + " )";
    error(errMsg, t);
}; };
var isDone = function (stream) {
    return isFinished(stream) || isEOF(peek(stream));
};
var find = function (arr, pred) {
    for (var i = 0; i < arr.length; i++) {
        var val = arr[i];
        if (pred(arr[i]))
            return val;
    }
    return undefined;
};
function parse(tokens) {
    if (tokens.length === 0)
        throw Error("Cannot parse empty token list.");
    if (tokens[tokens.length - 1].type !== TokenType.EOF)
        throw Error("Token list must end with EOF token.");
    var stream = createStream(tokens);
    var declarations = [];
    while (!isDone(stream)) {
        try {
            var d = parseDeclaration(stream, declarations);
            declarations.push(d);
        }
        catch (e) {
            if (!ParseError.is(e))
                throw e;
            pushError(e);
            break;
        }
    }
    return new File(declarations);
}
var resolve = function (id, declarations) {
    if (!isIdentifier(id))
        throw Error("Tried to resolve a variable with a token of type: " + id.type);
    var name = id.str;
    var varDecl = find(declarations, function (d) { return d.identifier.str === name; });
    if (varDecl === undefined)
        return undefined;
    return varDecl.value;
};
var parseDeclaration = function (stream, declarations) {
    var comment = null;
    if (isComment(peek(stream))) {
        comment = advance(stream).str;
    }
    var identifier = advanceIf(stream, isIdentifier, expected("a name"));
    if (isReserved(identifier.str))
        error(identifier.str + " is reserved. You can't use it to name things.", identifier);
    advanceIf(stream, isEquals, expected("="));
    var value = parseValue(stream, declarations);
    return new Declaration(identifier, value, comment);
};
var parseField = function (stream, declarations) {
    var comment = null;
    if (isComment(peek(stream))) {
        comment = advance(stream).str;
    }
    var identifier = advanceIf(stream, isIdentifier, expected("a name"));
    var isFieldIdentifier = false;
    var identifierToken = peek(stream);
    if (isStar(identifierToken)) {
        isFieldIdentifier = true;
        advance(stream);
    }
    advanceIf(stream, isColon, expected(":"));
    var value = parseValue(stream, declarations);
    if (isFieldIdentifier && value.type !== NodeType.String) {
        throw new ParseError("Only string fields can be identifiers. This field is of type: " + value.type, identifierToken);
    }
    return new Field(identifier, value, comment, isFieldIdentifier);
};
var parseValue = function (stream, declarations) {
    var token = peek(stream);
    if (isIdentifier(token)) {
        var v = resolve(peek(stream), declarations);
        if (v !== undefined) {
            advance(stream);
            return v;
        }
    }
    switch (token.type) {
        case TokenType.STR:
            return parseStringLiteral(stream);
        case TokenType.NUM:
            return parseNumberLiteral(stream);
        case TokenType.BOOL:
            return parseBoolLiteral(stream);
        default:
            return parseType(stream, declarations);
    }
};
var parseType = function (stream, declarations) {
    var t = peek(stream);
    if (isIdentifier(t)) {
        var v = resolve(t, declarations);
        if (v !== undefined) {
            if (isType(v)) {
                advance(stream);
                return v;
            }
            else {
                throw new ParseError(t.str + " does not reference a type", t);
            }
        }
    }
    switch (t.type) {
        case TokenType.OPEN_BRACE:
            return parseObject(stream, declarations);
        case TokenType.OPEN_BRACKET:
            return parseRange(stream);
        case TokenType.IDENTIFIER:
            switch (t.str) {
                case Reserved.List:
                    return parseList(stream, declarations);
                case Reserved.Set:
                    return parseSet(stream);
                case Reserved.Map:
                    return parseMap(stream, declarations);
                case Reserved.Bool:
                    return parseBool(stream, declarations);
                case Reserved.Number:
                    return parseNumber(stream, declarations);
                case Reserved.String:
                    return parseString(stream, declarations);
                case Reserved.Url:
                    return parseUrl(stream, declarations);
                case Reserved.Image:
                    return parseImage(stream, declarations);
                case Reserved.Video:
                    return parseVideo(stream, declarations);
                case Reserved.Sound:
                    return parseSound(stream, declarations);
                case Reserved.Ref:
                    return parseReference(stream, declarations);
                default:
                    throw new ParseError("Could not determine type to parse from token: " + t.str, t);
            }
        default:
            throw new ParseError("Could not generate type with " + printToken(t), t);
    }
};
var parseRange = function (stream, declarations) {
    advanceIf(stream, isOpenBracket, expected("["));
    var low = parseNumberLiteral(stream);
    var high = parseNumberLiteral(stream);
    if (low.value > high.value) {
        var t = low;
        low = high;
        high = t;
    }
    advanceIf(stream, isClosedBracket, expected("]"));
    return new Range(low.value, high.value);
};
var parseMap = function (stream, declarations) {
    advanceIf(stream, function (t) { return t.str === Reserved.Map; }, expected("the map keyword"));
    advanceIf(stream, isOpenParen, expected("("));
    var keyType;
    if (test(stream, function (t) { return t.str === Reserved.String; })) {
        keyType = parseString(stream, declarations);
    }
    else {
        keyType = parseReference(stream, declarations);
    }
    var valueType = parseType(stream, declarations);
    advanceIf(stream, isClosedParen, expected(")"));
    return new GMLMap(keyType, valueType);
};
var parseSet = function (stream) {
    advanceIf(stream, function (t) { return t.str === Reserved.Set; }, expected("the set keyword"));
    advanceIf(stream, isOpenParen, expected("("));
    var values = [];
    if (test(stream, isNumLiteral)) {
        while (!isDone(stream) && !test(stream, isClosedParen)) {
            var literal = advanceIf(stream, isNumLiteral, expected("a number"));
            var numValue = getNumValue(literal);
            values.push(numValue);
        }
    }
    else if (test(stream, isStrLiteral)) {
        while (!isDone(stream) && !test(stream, isClosedParen)) {
            var literal = advanceIf(stream, isStrLiteral, expected("a string"));
            values.push(literal.str);
        }
    }
    else {
        throw new ParseError("Expected a numeric or string literal.", peek(stream));
    }
    advanceIf(stream, isClosedParen, expected(")"));
    return new GMLSet(values);
};
var parseNumberLiteral = function (stream) {
    var num = advanceIf(stream, isNumLiteral, expected("a number"));
    var val = getNumValue(num);
    return new NumLiteral(val);
};
var parseStringLiteral = function (stream) {
    var str = advanceIf(stream, isStrLiteral, expected("a string"));
    return new StrLiteral(str.str);
};
var parseBoolLiteral = function (stream) {
    var bool = advanceIf(stream, isBoolLiteral, expected("true/false"));
    var val = getBoolValue(bool);
    return new BoolLiteral(val);
};
var parseObject = function (stream, topLevelDecls) {
    var fields = [];
    var openingBrace = advanceIf(stream, isOpenBrace, expected("{"));
    var numIdentifiers = 0;
    while (!isDone(stream)) {
        if (test(stream, isClosedBrace)) {
            advance(stream);
            return new Obj(fields);
        }
        var field = parseField(stream, topLevelDecls);
        if (field.isIdentifier)
            numIdentifiers++;
        if (numIdentifiers > 1)
            throw new ParseError("Duplicate identifier field. Objects can only have one identifier field", field.identifier);
        fields.push(field);
    }
    throw new ParseError("Could not find closing brace", openingBrace);
};
var parseList = function (stream, declarations) {
    var size = null;
    advanceIf(stream, function (t) { return t.str === Reserved.List; }, function (t) {
        throw new ParseError("Expected the list keyword but received " + t.str, t);
    });
    var openingParen = advanceIf(stream, isOpenParen, expected("("));
    var type;
    var typeToken = peek(stream);
    if (isIdentifier(typeToken)) {
        var v = resolve(typeToken, declarations);
        if (v) {
            if (isType(v)) {
                type = v;
                advance(stream);
            }
            else
                error(typeToken.str + " does not refer to a type. The first argument to a list must be a type.", typeToken);
        }
    }
    if (type === undefined) {
        type = parseType(stream, declarations);
    }
    var sizeToken = peek(stream);
    if (isIdentifier(sizeToken)) {
        var v = resolve(sizeToken, declarations);
        if (v) {
            if (NumLiteral.is(v)) {
                size = v.value;
                advance(stream);
            }
            else if (Range.is(v)) {
                size = v;
            }
            else {
                error(sizeToken.str + " does not refer to a numeric literal or range. The size argument to a list must be one of those two types.", sizeToken);
            }
        }
    }
    if (size === null) {
        if (test(stream, isNumLiteral)) {
            size = parseNumberLiteral(stream).value;
        }
        else if (test(stream, isOpenBracket)) {
            size = parseRange(stream);
        }
    }
    advanceIf(stream, isClosedParen, function () {
        error("Could not find closing parenthesis for list", openingParen);
    });
    return new List(type, size);
};
var literalParser = function (ctr, tokenPredicate, literalNodeType, valueTransformer) { return function (stream, declarations) {
    advance(stream);
    var defaultValue = null;
    if (test(stream, isOpenParen)) {
        advance(stream);
        if (test(stream, isIdentifier)) {
            var t = advance(stream);
            var v = resolve(t, declarations);
            if (v === undefined)
                throw new ParseError("Cannot resolve variable named: " + t.str, t);
            if (v.type === literalNodeType) {
                defaultValue = v.value;
            }
            else {
                throw new ParseError("Expected default value to be " + literalNodeType + " instead got " + v.type, t);
            }
        }
        else {
            var literal = advanceIf(stream, tokenPredicate, function () { return "Default values must match the type of the primitive."; });
            defaultValue = valueTransformer(literal);
        }
        advanceIf(stream, isClosedParen, function () { return "Default values must have a closing parenthesis."; });
    }
    return new ctr(defaultValue);
}; };
var parseNumber = literalParser(Num, isNumLiteral, NodeType.NumLiteral, getNumValue);
var parseBool = literalParser(Bool, isBoolLiteral, NodeType.BoolLiteral, getBoolValue);
var getStringValue = function (t) { return t.str; };
var parseString = literalParser(Str, isStrLiteral, NodeType.StrLiteral, getStringValue);
var parseUrl = literalParser(Url, isStrLiteral, NodeType.StrLiteral, getStringValue);
var parseVideo = literalParser(Video, isStrLiteral, NodeType.StrLiteral, getStringValue);
var parseImage = literalParser(Image, isStrLiteral, NodeType.StrLiteral, getStringValue);
var parseSound = literalParser(Sound, isStrLiteral, NodeType.StrLiteral, getStringValue);
var parseReference = function (stream, declarations) {
    advanceIf(stream, function (t) { return t.str === Reserved.Ref; }, function (t) { return error("References must start with 'ref'.", t); });
    advanceIf(stream, isOpenParen, expected("("));
    var t = peek(stream);
    if (!isIdentifier(t))
        error("References must start with an identifier.", t);
    var v = resolve(t, declarations);
    if (v === undefined)
        throw new ParseError("Reference does not refer to a known value.", t);
    if (!Obj.is(v))
        throw new ParseError("References are only allowed to point to objects.", t);
    if (find(v.fields, function (f) { return f.isIdentifier; }) === undefined) {
        throw new ParseError(t.str + " has no identifer (a field marked with a *). References can only point to fields with identifiers", t);
    }
    advance(stream);
    advanceIf(stream, isClosedParen, expected(")"));
    return new Ref(t.str);
};

function isDigit(char) {
    return char.match(/[0-9]/) !== null;
}
function isAlpha(char) {
    return char.match(/[A-Za-z_]/) !== null;
}
function lex(input) {
    if (input.length === 0)
        throw Error("Empty input");
    var stream = createStream(input);
    var tokens = [];
    var start = 0;
    var createToken = function (type, str) {
        if (type === undefined)
            throw Error("Undefined is not a valid token type");
        tokens.push({
            type: type,
            offset: start,
            str: str === undefined ? getString() : str
        });
    };
    var getString = function () { return input.substring(start, stream.current); };
    var number = function () {
        while (test(stream, isDigit))
            advance(stream);
        if (test(stream, function (t) { return t === "."; }) && testAhead(stream, 1, isDigit)) {
            advance(stream);
            while (test(stream, isDigit))
                advance(stream);
        }
        createToken(TokenType.NUM);
    };
    var boundedToken = function (boundaryChar, type, terminationError) {
        while (!test(stream, function (t) { return t === boundaryChar; }) && !isFinished(stream))
            advance(stream);
        if (isFinished(stream)) {
            pushError(new terminationError(stream.current));
            return;
        }
        advance(stream);
        createToken(type, input.substring(start + 1, stream.current - 1));
    };
    var string = function () {
        return boundedToken('"', TokenType.STR, UnterminatedStringError);
    };
    var comment = function () {
        advanceIf(stream, function (c) { return c === "<"; }, function (c) {
            return pushError(new InvalidTokenError("Expected a < to complete a comment start but got a " + c, stream.current));
        });
        var closedPredicate = function (c) { return c === ">"; };
        while (!isFinished(stream) &&
            !(test(stream, closedPredicate) &&
                testAhead(stream, 1, closedPredicate)))
            advance(stream);
        if (isFinished(stream)) {
            pushError(new UnterminatedCommentError(stream.current));
            return;
        }
        for (var i = 0; i < 2; i++) {
            if (test(stream, function (c) { return c === ">"; })) {
                advance(stream);
            }
            else {
                if (isFinished(stream)) {
                    pushError(new InvalidTokenError("Expected a > to complete a comment end but file unexpectedly ended", stream.current));
                }
                else {
                    pushError(new InvalidTokenError("Expected a > to complete a comment end but got a " + peek(stream), stream.current));
                }
            }
        }
        createToken(TokenType.COMMENT, input.substring(start + 2, stream.current - 2));
    };
    var identifier = function () {
        while (test(stream, isAlpha) || test(stream, isDigit))
            advance(stream);
        var str = getString();
        if (str === Reserved.True || str === Reserved.False) {
            createToken(TokenType.BOOL, str);
        }
        else {
            createToken(TokenType.IDENTIFIER, str);
        }
    };
    while (!isFinished(stream)) {
        start = stream.current;
        var c = advance(stream);
        switch (c) {
            case " ":
            case "\t":
            case "\r":
            case "\n":
                break;
            case "(":
                createToken(TokenType.OPEN_PAREN);
                break;
            case ")":
                createToken(TokenType.CLOSED_PAREN);
                break;
            case "{":
                createToken(TokenType.OPEN_BRACE);
                break;
            case "}":
                createToken(TokenType.CLOSED_BRACE);
                break;
            case "[":
                createToken(TokenType.OPEN_BRACKET);
                break;
            case "]":
                createToken(TokenType.CLOSED_BRACKET);
                break;
            case ":":
                createToken(TokenType.COLON);
                break;
            case "=":
                createToken(TokenType.EQUALS);
                break;
            case ",":
                createToken(TokenType.COMMA);
                break;
            case "*":
                createToken(TokenType.STAR);
                break;
            default:
                if (c === "<") {
                    comment();
                }
                else if (c === '"') {
                    string();
                }
                else if (isDigit(c)) {
                    number();
                }
                else if (isAlpha(c)) {
                    identifier();
                }
                else {
                    pushError(new InvalidTokenError("Unrecognized character: " + c, stream.current));
                }
        }
    }
    if (tokens.length === 0 && getErrors().length === 0)
        throw Error("Empty input");
    tokens.push({ type: TokenType.EOF, str: "EOF", offset: stream.current });
    return tokens;
}

// Unique ID creation requires a high quality random # generator.  In node.js
// this is pretty straight-forward - we use the crypto API.



var rng = function nodeRNG() {
  return crypto.randomBytes(16);
};

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];
for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex;
  // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
  return ([bth[buf[i++]], bth[buf[i++]], 
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]]]).join('');
}

var bytesToUuid_1 = bytesToUuid;

// **`v1()` - Generate time-based UUID**
//
// Inspired by https://github.com/LiosK/UUID.js
// and http://docs.python.org/library/uuid.html

var _nodeId;
var _clockseq;

// Previous uuid creation time
var _lastMSecs = 0;
var _lastNSecs = 0;

// See https://github.com/broofa/node-uuid for API details
function v1(options, buf, offset) {
  var i = buf && offset || 0;
  var b = buf || [];

  options = options || {};
  var node = options.node || _nodeId;
  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

  // node and clockseq need to be initialized to random values if they're not
  // specified.  We do this lazily to minimize issues related to insufficient
  // system entropy.  See #189
  if (node == null || clockseq == null) {
    var seedBytes = rng();
    if (node == null) {
      // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
      node = _nodeId = [
        seedBytes[0] | 0x01,
        seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]
      ];
    }
    if (clockseq == null) {
      // Per 4.2.2, randomize (14 bit) clockseq
      clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
    }
  }

  // UUID timestamps are 100 nano-second units since the Gregorian epoch,
  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

  // Per 4.2.1.2, use count of uuid's generated during the current clock
  // cycle to simulate higher resolution clock
  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

  // Time since last uuid creation (in msecs)
  var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

  // Per 4.2.1.2, Bump clockseq on clock regression
  if (dt < 0 && options.clockseq === undefined) {
    clockseq = clockseq + 1 & 0x3fff;
  }

  // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
  // time interval
  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
    nsecs = 0;
  }

  // Per 4.2.1.2 Throw error if too many uuids are requested
  if (nsecs >= 10000) {
    throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
  }

  _lastMSecs = msecs;
  _lastNSecs = nsecs;
  _clockseq = clockseq;

  // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
  msecs += 12219292800000;

  // `time_low`
  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
  b[i++] = tl >>> 24 & 0xff;
  b[i++] = tl >>> 16 & 0xff;
  b[i++] = tl >>> 8 & 0xff;
  b[i++] = tl & 0xff;

  // `time_mid`
  var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
  b[i++] = tmh >>> 8 & 0xff;
  b[i++] = tmh & 0xff;

  // `time_high_and_version`
  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
  b[i++] = tmh >>> 16 & 0xff;

  // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
  b[i++] = clockseq >>> 8 | 0x80;

  // `clock_seq_low`
  b[i++] = clockseq & 0xff;

  // `node`
  for (var n = 0; n < 6; ++n) {
    b[i + n] = node[n];
  }

  return buf ? buf : bytesToUuid_1(b);
}

var v1_1 = v1;

function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof(options) == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }
  options = options || {};

  var rnds = options.random || (options.rng || rng)();

  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
  rnds[6] = (rnds[6] & 0x0f) | 0x40;
  rnds[8] = (rnds[8] & 0x3f) | 0x80;

  // Copy bytes to buffer, if provided
  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || bytesToUuid_1(rnds);
}

var v4_1 = v4;

var uuid = v4_1;
uuid.v1 = v1_1;
uuid.v4 = v4_1;

var uuid_1 = uuid;

var headers = "import { types, SnapshotIn, SnapshotOut, Instance } from \"mobx-state-tree\";";
var helperFunctions = "\nconst isOfLength = (l: number) => (snap: { length: number } | undefined) => {\n    if (snap === undefined) return false;\n    return snap.length === l;\n};\n\nconst isInRange = (low: number, high:number) => (snap: { length: number } | undefined) => {\n    if (snap === undefined) return false;\n    const l = snap.length;\n    return l >= low && l <= high;\n};\n";
function render(file) {
    return "/*\nTHIS FILE WAS GENERATED BY A SCRIPT.\nDO NOT MODIFY IT DIRECTLY.\n*/    \n" + headers + "\n\n// HELPER FUNCTIONS\n" + helperFunctions + "\n\n// DATA\n" + renderFile(file) + "\n\n\n// PLACEHOLDERS\n" + renderPlaceholders(placeholders) + "\n";
}
// Maps the values to declaration names
var valToDeclName = {};
var placeholders = {};
var p = "";
function renderPlaceholders(placeholders) {
    var strings = Object.keys(placeholders).map(function (k) {
        var p = placeholders[k];
        var n = k.split("Model")[0];
        return "export function createDefault" + n + "():" + n + "SnapOut {return " + p + "}";
    });
    return strings.join("\n");
}
function renderFile(file) {
    placeholders = {};
    var declarationStrings = [];
    file.declarations.forEach(function (d) { return declarationStrings.push(renderDecl(d)); });
    return declarationStrings.join("\n");
}
function renderDecl(decl) {
    var baseName = decl.identifier.str;
    var name = baseName;
    if (Obj.is(decl.value)) {
        name = name + "Model";
        p = "";
    }
    var value = renderValue(decl.value);
    var out;
    if (Obj.is(decl.value)) {
        out = "export const " + name + " = " + value + ".named(\"" + name + "\");\nexport type " + baseName + "SnapIn = SnapshotIn<typeof " + name + ">;\nexport type " + baseName + "SnapOut = SnapshotOut<typeof " + name + ">;\nexport type " + baseName + " = Instance<typeof " + name + ">;\n";
        placeholders[name] = p;
    }
    else {
        out = "export const " + name + " = " + value + ";";
    }
    valToDeclName[value] = name;
    if (decl.comment)
        out = "\n// " + decl.comment + "\n" + out;
    return out;
}
function renderField(field) {
    var comment = "" + (field.comment ? "// " + field.comment + "\n" : "");
    var name = "" + field.identifier.str;
    if (field.isIdentifier) {
        p += " " + name + ": \"" + uuid_1.v4() + "\",";
        return "" + comment + name + ": types.identifier,";
    }
    else {
        p += " " + name + ":";
        var val = renderValue(field.value);
        p += ",";
        return "" + comment + name + ": " + val + ",";
    }
}
function renderValue(node) {
    var result;
    switch (node.type) {
        // Literals
        case NodeType.NumLiteral:
            result = renderNumLiteral(node);
            break;
        case NodeType.StrLiteral:
            result = renderStrLiteral(node);
            break;
        case NodeType.BoolLiteral:
            result = renderBoolLiteral(node);
            break;
        // Types
        case NodeType.Number:
        case NodeType.String:
        case NodeType.Bool:
        case NodeType.Url:
        case NodeType.Image:
        case NodeType.Video:
        case NodeType.Sound:
        case NodeType.Range:
        case NodeType.Reference:
        case NodeType.Object:
        case NodeType.List:
        case NodeType.Set:
        case NodeType.Map:
            return renderType(node);
        default:
            throw new Error("Invalid value type: " + node.type);
    }
    var cached = valToDeclName[result];
    if (cached !== undefined)
        return cached;
    return result;
}
function renderNumLiteral(node) {
    if (!NumLiteral.is(node))
        throw new Error("Expected a num literal but got a: " + node.type);
    p += node.value;
    return "" + node.value;
}
function renderStrLiteral(node) {
    if (!StrLiteral.is(node))
        throw new Error("Expected a str literal but got a: " + node.type);
    p += "\"" + node.value + "\"";
    return "" + node.value;
}
function renderBoolLiteral(node) {
    if (!BoolLiteral.is(node))
        throw new Error("Expected a bool literal but got a: " + node.type);
    p += node.value;
    return "" + node.value;
}
function renderType(node) {
    var result;
    switch (node.type) {
        case NodeType.Number:
            result = renderNumber(node);
            break;
        case NodeType.String:
            result = renderString(node);
            break;
        case NodeType.Bool:
            result = renderBool(node);
            break;
        case NodeType.Url:
            result = renderUrl(node);
            break;
        case NodeType.Image:
            result = renderImage(node);
            break;
        case NodeType.Video:
            result = renderVideo(node);
            break;
        case NodeType.Sound:
            result = renderSound(node);
            break;
        case NodeType.Range:
            result = renderRange(node);
            break;
        case NodeType.Reference:
            result = renderReference(node);
            break;
        case NodeType.Object:
            result = renderObj(node);
            break;
        case NodeType.List:
            result = renderList(node);
            break;
        case NodeType.Set:
            result = renderSet(node);
            break;
        case NodeType.Map:
            result = renderMap(node);
            break;
        default:
            throw new Error("Invalid type type: " + node.type);
    }
    var cached = valToDeclName[result];
    if (cached !== undefined)
        return cached;
    return result;
}
function renderNumber(node) {
    if (!Num.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += node.defaultValue;
        return "" + node.defaultValue;
    }
    else {
        p += -1;
        return "types.number";
    }
}
function renderBool(node) {
    if (!Bool.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += node.defaultValue;
        return "" + node.defaultValue;
    }
    else {
        p += "false";
        return "types.boolean";
    }
}
function renderString(node) {
    if (!Str.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this string\"";
        return "types.string";
    }
}
function renderUrl(node) {
    if (!Url.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this url\"";
        return "types.string";
    }
}
function renderImage(node) {
    if (!Image.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this image url\"";
        return "types.string";
    }
}
function renderVideo(node) {
    if (!Video.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this video url\"";
        return "types.string";
    }
}
function renderSound(node) {
    if (!Sound.is(node))
        throw new Error("Unexpected node type");
    if (node.defaultValue) {
        p += "\"" + node.defaultValue + "\"";
        return "types.optional(types.string, \"" + node.defaultValue + "\")";
    }
    else {
        p += "\"Fill in this sound url\"";
        return "types.string";
    }
}
function renderRange(node) {
    if (!Range.is(node))
        throw new Error("Unexpected node type");
    p += node.lowerBound;
    return "types.refinement(types.number, isInRange(" + node.lowerBound + "," + node.upperBound + "))";
}
function renderReference(node) {
    if (!Ref.is(node))
        throw new Error("Unexpected node type");
    p += "\"" + node.identifierName + "\"";
    return "types.reference(" + node.identifierName + "Model)";
}
function renderSet(node) {
    if (!GMLSet.is(node))
        throw new Error("Unexpected node type");
    var values = node.values;
    var valueStrings;
    var val = node.values[0];
    if (typeof val === "string") {
        p += "\"" + val + "\"";
        valueStrings = values.map(function (v) { return "types.literal(\"" + v + "\")"; });
    }
    else if (typeof values[0] === "number") {
        p += val;
        valueStrings = values.map(function (v) { return "types.literal(" + v + ")"; });
    }
    else
        throw new Error("Unexpected set type");
    return "types.union(" + valueStrings.join(",") + ")";
}
function renderMap(node) {
    if (!GMLMap.is(node))
        throw new Error("Unexpected node type");
    var name = "Key";
    if (Ref.is(node.keyType)) {
        name = node.keyType.identifierName;
    }
    else if (Str.is(node.keyType)) {
        name = node.keyType.defaultValue || "Key";
    }
    else
        throw new Error("Unexpected map key type");
    p += "{" + name + ":";
    var type = renderType(node.valueType);
    p += "}";
    return "types.map(" + type + ")";
}
function renderObj(obj) {
    if (!Obj.is(obj))
        throw new Error("Unexpected type");
    var fieldsStrings = [];
    p += "{";
    obj.fields.forEach(function (f) { return fieldsStrings.push(renderField(f)); });
    var fields = fieldsStrings.join("\n");
    p += "}";
    return "types.model({\n" + fields + "\n})";
}
function renderList(list) {
    if (!List.is(list))
        throw new Error("Unexpected type");
    var out;
    p += "[";
    var previous = p;
    p = "";
    var array = "types.array(" + renderType(list.typeParam) + ")";
    var element = p;
    var size;
    if (Range.is(list.size)) {
        size = list.size.lowerBound;
        out = "types.refinement(" + array + ", isInRange(" + list.size.lowerBound + ", " + list.size.upperBound + "))";
    }
    else if (typeof list.size === "number") {
        var value = "" + list.size;
        var cached = valToDeclName[value];
        if (cached !== undefined)
            value = cached;
        size = list.size;
        out = "types.refinement(" + array + ", isOfLength(" + value + "))";
    }
    else {
        size = 1;
        out = array;
    }
    p = previous;
    var elements = [];
    for (var i = 0; i < size; i++)
        elements.push(element);
    p += elements.join(",");
    p += "]";
    return out;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var commander = createCommonjsModule(function (module, exports) {
/**
 * Module dependencies.
 */

var EventEmitter = events.EventEmitter;
var spawn = child_process.spawn;

var dirname = path.dirname;
var basename = path.basename;


/**
 * Inherit `Command` from `EventEmitter.prototype`.
 */

util.inherits(Command, EventEmitter);

/**
 * Expose the root command.
 */

exports = module.exports = new Command();

/**
 * Expose `Command`.
 */

exports.Command = Command;

/**
 * Expose `Option`.
 */

exports.Option = Option;

/**
 * Initialize a new `Option` with the given `flags` and `description`.
 *
 * @param {String} flags
 * @param {String} description
 * @api public
 */

function Option(flags, description) {
  this.flags = flags;
  this.required = flags.indexOf('<') >= 0;
  this.optional = flags.indexOf('[') >= 0;
  this.bool = flags.indexOf('-no-') === -1;
  flags = flags.split(/[ ,|]+/);
  if (flags.length > 1 && !/^[[<]/.test(flags[1])) this.short = flags.shift();
  this.long = flags.shift();
  this.description = description || '';
}

/**
 * Return option name.
 *
 * @return {String}
 * @api private
 */

Option.prototype.name = function() {
  return this.long
    .replace('--', '')
    .replace('no-', '');
};

/**
 * Return option name, in a camelcase format that can be used
 * as a object attribute key.
 *
 * @return {String}
 * @api private
 */

Option.prototype.attributeName = function() {
  return camelcase(this.name());
};

/**
 * Check if `arg` matches the short or long flag.
 *
 * @param {String} arg
 * @return {Boolean}
 * @api private
 */

Option.prototype.is = function(arg) {
  return this.short === arg || this.long === arg;
};

/**
 * Initialize a new `Command`.
 *
 * @param {String} name
 * @api public
 */

function Command(name) {
  this.commands = [];
  this.options = [];
  this._execs = {};
  this._allowUnknownOption = false;
  this._args = [];
  this._name = name || '';
}

/**
 * Add command `name`.
 *
 * The `.action()` callback is invoked when the
 * command `name` is specified via __ARGV__,
 * and the remaining arguments are applied to the
 * function for access.
 *
 * When the `name` is "*" an un-matched command
 * will be passed as the first arg, followed by
 * the rest of __ARGV__ remaining.
 *
 * Examples:
 *
 *      program
 *        .version('0.0.1')
 *        .option('-C, --chdir <path>', 'change the working directory')
 *        .option('-c, --config <path>', 'set config path. defaults to ./deploy.conf')
 *        .option('-T, --no-tests', 'ignore test hook')
 *
 *      program
 *        .command('setup')
 *        .description('run remote setup commands')
 *        .action(function() {
 *          console.log('setup');
 *        });
 *
 *      program
 *        .command('exec <cmd>')
 *        .description('run the given remote command')
 *        .action(function(cmd) {
 *          console.log('exec "%s"', cmd);
 *        });
 *
 *      program
 *        .command('teardown <dir> [otherDirs...]')
 *        .description('run teardown commands')
 *        .action(function(dir, otherDirs) {
 *          console.log('dir "%s"', dir);
 *          if (otherDirs) {
 *            otherDirs.forEach(function (oDir) {
 *              console.log('dir "%s"', oDir);
 *            });
 *          }
 *        });
 *
 *      program
 *        .command('*')
 *        .description('deploy the given env')
 *        .action(function(env) {
 *          console.log('deploying "%s"', env);
 *        });
 *
 *      program.parse(process.argv);
  *
 * @param {String} name
 * @param {String} [desc] for git-style sub-commands
 * @return {Command} the new command
 * @api public
 */

Command.prototype.command = function(name, desc, opts) {
  if (typeof desc === 'object' && desc !== null) {
    opts = desc;
    desc = null;
  }
  opts = opts || {};
  var args = name.split(/ +/);
  var cmd = new Command(args.shift());

  if (desc) {
    cmd.description(desc);
    this.executables = true;
    this._execs[cmd._name] = true;
    if (opts.isDefault) this.defaultExecutable = cmd._name;
  }
  cmd._noHelp = !!opts.noHelp;
  this.commands.push(cmd);
  cmd.parseExpectedArgs(args);
  cmd.parent = this;

  if (desc) return this;
  return cmd;
};

/**
 * Define argument syntax for the top-level command.
 *
 * @api public
 */

Command.prototype.arguments = function(desc) {
  return this.parseExpectedArgs(desc.split(/ +/));
};

/**
 * Add an implicit `help [cmd]` subcommand
 * which invokes `--help` for the given command.
 *
 * @api private
 */

Command.prototype.addImplicitHelpCommand = function() {
  this.command('help [cmd]', 'display help for [cmd]');
};

/**
 * Parse expected `args`.
 *
 * For example `["[type]"]` becomes `[{ required: false, name: 'type' }]`.
 *
 * @param {Array} args
 * @return {Command} for chaining
 * @api public
 */

Command.prototype.parseExpectedArgs = function(args) {
  if (!args.length) return;
  var self = this;
  args.forEach(function(arg) {
    var argDetails = {
      required: false,
      name: '',
      variadic: false
    };

    switch (arg[0]) {
      case '<':
        argDetails.required = true;
        argDetails.name = arg.slice(1, -1);
        break;
      case '[':
        argDetails.name = arg.slice(1, -1);
        break;
    }

    if (argDetails.name.length > 3 && argDetails.name.slice(-3) === '...') {
      argDetails.variadic = true;
      argDetails.name = argDetails.name.slice(0, -3);
    }
    if (argDetails.name) {
      self._args.push(argDetails);
    }
  });
  return this;
};

/**
 * Register callback `fn` for the command.
 *
 * Examples:
 *
 *      program
 *        .command('help')
 *        .description('display verbose help')
 *        .action(function() {
 *           // output help here
 *        });
 *
 * @param {Function} fn
 * @return {Command} for chaining
 * @api public
 */

Command.prototype.action = function(fn) {
  var self = this;
  var listener = function(args, unknown) {
    // Parse any so-far unknown options
    args = args || [];
    unknown = unknown || [];

    var parsed = self.parseOptions(unknown);

    // Output help if necessary
    outputHelpIfNecessary(self, parsed.unknown);

    // If there are still any unknown options, then we simply
    // die, unless someone asked for help, in which case we give it
    // to them, and then we die.
    if (parsed.unknown.length > 0) {
      self.unknownOption(parsed.unknown[0]);
    }

    // Leftover arguments need to be pushed back. Fixes issue #56
    if (parsed.args.length) args = parsed.args.concat(args);

    self._args.forEach(function(arg, i) {
      if (arg.required && args[i] == null) {
        self.missingArgument(arg.name);
      } else if (arg.variadic) {
        if (i !== self._args.length - 1) {
          self.variadicArgNotLast(arg.name);
        }

        args[i] = args.splice(i);
      }
    });

    // Always append ourselves to the end of the arguments,
    // to make sure we match the number of arguments the user
    // expects
    if (self._args.length) {
      args[self._args.length] = self;
    } else {
      args.push(self);
    }

    fn.apply(self, args);
  };
  var parent = this.parent || this;
  var name = parent === this ? '*' : this._name;
  parent.on('command:' + name, listener);
  if (this._alias) parent.on('command:' + this._alias, listener);
  return this;
};

/**
 * Define option with `flags`, `description` and optional
 * coercion `fn`.
 *
 * The `flags` string should contain both the short and long flags,
 * separated by comma, a pipe or space. The following are all valid
 * all will output this way when `--help` is used.
 *
 *    "-p, --pepper"
 *    "-p|--pepper"
 *    "-p --pepper"
 *
 * Examples:
 *
 *     // simple boolean defaulting to false
 *     program.option('-p, --pepper', 'add pepper');
 *
 *     --pepper
 *     program.pepper
 *     // => Boolean
 *
 *     // simple boolean defaulting to true
 *     program.option('-C, --no-cheese', 'remove cheese');
 *
 *     program.cheese
 *     // => true
 *
 *     --no-cheese
 *     program.cheese
 *     // => false
 *
 *     // required argument
 *     program.option('-C, --chdir <path>', 'change the working directory');
 *
 *     --chdir /tmp
 *     program.chdir
 *     // => "/tmp"
 *
 *     // optional argument
 *     program.option('-c, --cheese [type]', 'add cheese [marble]');
 *
 * @param {String} flags
 * @param {String} description
 * @param {Function|*} [fn] or default
 * @param {*} [defaultValue]
 * @return {Command} for chaining
 * @api public
 */

Command.prototype.option = function(flags, description, fn, defaultValue) {
  var self = this,
    option = new Option(flags, description),
    oname = option.name(),
    name = option.attributeName();

  // default as 3rd arg
  if (typeof fn !== 'function') {
    if (fn instanceof RegExp) {
      var regex = fn;
      fn = function(val, def) {
        var m = regex.exec(val);
        return m ? m[0] : def;
      };
    } else {
      defaultValue = fn;
      fn = null;
    }
  }

  // preassign default value only for --no-*, [optional], or <required>
  if (!option.bool || option.optional || option.required) {
    // when --no-* we make sure default is true
    if (!option.bool) defaultValue = true;
    // preassign only if we have a default
    if (defaultValue !== undefined) {
      self[name] = defaultValue;
      option.defaultValue = defaultValue;
    }
  }

  // register the option
  this.options.push(option);

  // when it's passed assign the value
  // and conditionally invoke the callback
  this.on('option:' + oname, function(val) {
    // coercion
    if (val !== null && fn) {
      val = fn(val, self[name] === undefined ? defaultValue : self[name]);
    }

    // unassigned or bool
    if (typeof self[name] === 'boolean' || typeof self[name] === 'undefined') {
      // if no value, bool true, and we have a default, then use it!
      if (val == null) {
        self[name] = option.bool
          ? defaultValue || true
          : false;
      } else {
        self[name] = val;
      }
    } else if (val !== null) {
      // reassign
      self[name] = val;
    }
  });

  return this;
};

/**
 * Allow unknown options on the command line.
 *
 * @param {Boolean} arg if `true` or omitted, no error will be thrown
 * for unknown options.
 * @api public
 */
Command.prototype.allowUnknownOption = function(arg) {
  this._allowUnknownOption = arguments.length === 0 || arg;
  return this;
};

/**
 * Parse `argv`, settings options and invoking commands when defined.
 *
 * @param {Array} argv
 * @return {Command} for chaining
 * @api public
 */

Command.prototype.parse = function(argv) {
  // implicit help
  if (this.executables) this.addImplicitHelpCommand();

  // store raw args
  this.rawArgs = argv;

  // guess name
  this._name = this._name || basename(argv[1], '.js');

  // github-style sub-commands with no sub-command
  if (this.executables && argv.length < 3 && !this.defaultExecutable) {
    // this user needs help
    argv.push('--help');
  }

  // process argv
  var parsed = this.parseOptions(this.normalize(argv.slice(2)));
  var args = this.args = parsed.args;

  var result = this.parseArgs(this.args, parsed.unknown);

  // executable sub-commands
  var name = result.args[0];

  var aliasCommand = null;
  // check alias of sub commands
  if (name) {
    aliasCommand = this.commands.filter(function(command) {
      return command.alias() === name;
    })[0];
  }

  if (this._execs[name] && typeof this._execs[name] !== 'function') {
    return this.executeSubCommand(argv, args, parsed.unknown);
  } else if (aliasCommand) {
    // is alias of a subCommand
    args[0] = aliasCommand._name;
    return this.executeSubCommand(argv, args, parsed.unknown);
  } else if (this.defaultExecutable) {
    // use the default subcommand
    args.unshift(this.defaultExecutable);
    return this.executeSubCommand(argv, args, parsed.unknown);
  }

  return result;
};

/**
 * Execute a sub-command executable.
 *
 * @param {Array} argv
 * @param {Array} args
 * @param {Array} unknown
 * @api private
 */

Command.prototype.executeSubCommand = function(argv, args, unknown) {
  args = args.concat(unknown);

  if (!args.length) this.help();
  if (args[0] === 'help' && args.length === 1) this.help();

  // <cmd> --help
  if (args[0] === 'help') {
    args[0] = args[1];
    args[1] = '--help';
  }

  // executable
  var f = argv[1];
  // name of the subcommand, link `pm-install`
  var bin = basename(f, path.extname(f)) + '-' + args[0];

  // In case of globally installed, get the base dir where executable
  //  subcommand file should be located at
  var baseDir;

  var resolvedLink = fs__default.realpathSync(f);

  baseDir = dirname(resolvedLink);

  // prefer local `./<bin>` to bin in the $PATH
  var localBin = path.join(baseDir, bin);

  // whether bin file is a js script with explicit `.js` or `.ts` extension
  var isExplicitJS = false;
  if (exists(localBin + '.js')) {
    bin = localBin + '.js';
    isExplicitJS = true;
  } else if (exists(localBin + '.ts')) {
    bin = localBin + '.ts';
    isExplicitJS = true;
  } else if (exists(localBin)) {
    bin = localBin;
  }

  args = args.slice(1);

  var proc;
  if (process.platform !== 'win32') {
    if (isExplicitJS) {
      args.unshift(bin);
      // add executable arguments to spawn
      args = (process.execArgv || []).concat(args);

      proc = spawn(process.argv[0], args, { stdio: 'inherit', customFds: [0, 1, 2] });
    } else {
      proc = spawn(bin, args, { stdio: 'inherit', customFds: [0, 1, 2] });
    }
  } else {
    args.unshift(bin);
    proc = spawn(process.execPath, args, { stdio: 'inherit' });
  }

  var signals = ['SIGUSR1', 'SIGUSR2', 'SIGTERM', 'SIGINT', 'SIGHUP'];
  signals.forEach(function(signal) {
    process.on(signal, function() {
      if (proc.killed === false && proc.exitCode === null) {
        proc.kill(signal);
      }
    });
  });
  proc.on('close', process.exit.bind(process));
  proc.on('error', function(err) {
    if (err.code === 'ENOENT') {
      console.error('error: %s(1) does not exist, try --help', bin);
    } else if (err.code === 'EACCES') {
      console.error('error: %s(1) not executable. try chmod or run with root', bin);
    }
    process.exit(1);
  });

  // Store the reference to the child process
  this.runningCommand = proc;
};

/**
 * Normalize `args`, splitting joined short flags. For example
 * the arg "-abc" is equivalent to "-a -b -c".
 * This also normalizes equal sign and splits "--abc=def" into "--abc def".
 *
 * @param {Array} args
 * @return {Array}
 * @api private
 */

Command.prototype.normalize = function(args) {
  var ret = [],
    arg,
    lastOpt,
    index;

  for (var i = 0, len = args.length; i < len; ++i) {
    arg = args[i];
    if (i > 0) {
      lastOpt = this.optionFor(args[i - 1]);
    }

    if (arg === '--') {
      // Honor option terminator
      ret = ret.concat(args.slice(i));
      break;
    } else if (lastOpt && lastOpt.required) {
      ret.push(arg);
    } else if (arg.length > 1 && arg[0] === '-' && arg[1] !== '-') {
      arg.slice(1).split('').forEach(function(c) {
        ret.push('-' + c);
      });
    } else if (/^--/.test(arg) && ~(index = arg.indexOf('='))) {
      ret.push(arg.slice(0, index), arg.slice(index + 1));
    } else {
      ret.push(arg);
    }
  }

  return ret;
};

/**
 * Parse command `args`.
 *
 * When listener(s) are available those
 * callbacks are invoked, otherwise the "*"
 * event is emitted and those actions are invoked.
 *
 * @param {Array} args
 * @return {Command} for chaining
 * @api private
 */

Command.prototype.parseArgs = function(args, unknown) {
  var name;

  if (args.length) {
    name = args[0];
    if (this.listeners('command:' + name).length) {
      this.emit('command:' + args.shift(), args, unknown);
    } else {
      this.emit('command:*', args);
    }
  } else {
    outputHelpIfNecessary(this, unknown);

    // If there were no args and we have unknown options,
    // then they are extraneous and we need to error.
    if (unknown.length > 0) {
      this.unknownOption(unknown[0]);
    }
    if (this.commands.length === 0 &&
        this._args.filter(function(a) { return a.required; }).length === 0) {
      this.emit('command:*');
    }
  }

  return this;
};

/**
 * Return an option matching `arg` if any.
 *
 * @param {String} arg
 * @return {Option}
 * @api private
 */

Command.prototype.optionFor = function(arg) {
  for (var i = 0, len = this.options.length; i < len; ++i) {
    if (this.options[i].is(arg)) {
      return this.options[i];
    }
  }
};

/**
 * Parse options from `argv` returning `argv`
 * void of these options.
 *
 * @param {Array} argv
 * @return {Array}
 * @api public
 */

Command.prototype.parseOptions = function(argv) {
  var args = [],
    len = argv.length,
    literal,
    option,
    arg;

  var unknownOptions = [];

  // parse options
  for (var i = 0; i < len; ++i) {
    arg = argv[i];

    // literal args after --
    if (literal) {
      args.push(arg);
      continue;
    }

    if (arg === '--') {
      literal = true;
      continue;
    }

    // find matching Option
    option = this.optionFor(arg);

    // option is defined
    if (option) {
      // requires arg
      if (option.required) {
        arg = argv[++i];
        if (arg == null) return this.optionMissingArgument(option);
        this.emit('option:' + option.name(), arg);
      // optional arg
      } else if (option.optional) {
        arg = argv[i + 1];
        if (arg == null || (arg[0] === '-' && arg !== '-')) {
          arg = null;
        } else {
          ++i;
        }
        this.emit('option:' + option.name(), arg);
      // bool
      } else {
        this.emit('option:' + option.name());
      }
      continue;
    }

    // looks like an option
    if (arg.length > 1 && arg[0] === '-') {
      unknownOptions.push(arg);

      // If the next argument looks like it might be
      // an argument for this option, we pass it on.
      // If it isn't, then it'll simply be ignored
      if ((i + 1) < argv.length && argv[i + 1][0] !== '-') {
        unknownOptions.push(argv[++i]);
      }
      continue;
    }

    // arg
    args.push(arg);
  }

  return { args: args, unknown: unknownOptions };
};

/**
 * Return an object containing options as key-value pairs
 *
 * @return {Object}
 * @api public
 */
Command.prototype.opts = function() {
  var result = {},
    len = this.options.length;

  for (var i = 0; i < len; i++) {
    var key = this.options[i].attributeName();
    result[key] = key === this._versionOptionName ? this._version : this[key];
  }
  return result;
};

/**
 * Argument `name` is missing.
 *
 * @param {String} name
 * @api private
 */

Command.prototype.missingArgument = function(name) {
  console.error("error: missing required argument `%s'", name);
  process.exit(1);
};

/**
 * `Option` is missing an argument, but received `flag` or nothing.
 *
 * @param {String} option
 * @param {String} flag
 * @api private
 */

Command.prototype.optionMissingArgument = function(option, flag) {
  if (flag) {
    console.error("error: option `%s' argument missing, got `%s'", option.flags, flag);
  } else {
    console.error("error: option `%s' argument missing", option.flags);
  }
  process.exit(1);
};

/**
 * Unknown option `flag`.
 *
 * @param {String} flag
 * @api private
 */

Command.prototype.unknownOption = function(flag) {
  if (this._allowUnknownOption) return;
  console.error("error: unknown option `%s'", flag);
  process.exit(1);
};

/**
 * Variadic argument with `name` is not the last argument as required.
 *
 * @param {String} name
 * @api private
 */

Command.prototype.variadicArgNotLast = function(name) {
  console.error("error: variadic arguments must be last `%s'", name);
  process.exit(1);
};

/**
 * Set the program version to `str`.
 *
 * This method auto-registers the "-V, --version" flag
 * which will print the version number when passed.
 *
 * @param {String} str
 * @param {String} [flags]
 * @return {Command} for chaining
 * @api public
 */

Command.prototype.version = function(str, flags) {
  if (arguments.length === 0) return this._version;
  this._version = str;
  flags = flags || '-V, --version';
  var versionOption = new Option(flags, 'output the version number');
  this._versionOptionName = versionOption.long.substr(2) || 'version';
  this.options.push(versionOption);
  this.on('option:' + this._versionOptionName, function() {
    process.stdout.write(str + '\n');
    process.exit(0);
  });
  return this;
};

/**
 * Set the description to `str`.
 *
 * @param {String} str
 * @param {Object} argsDescription
 * @return {String|Command}
 * @api public
 */

Command.prototype.description = function(str, argsDescription) {
  if (arguments.length === 0) return this._description;
  this._description = str;
  this._argsDescription = argsDescription;
  return this;
};

/**
 * Set an alias for the command
 *
 * @param {String} alias
 * @return {String|Command}
 * @api public
 */

Command.prototype.alias = function(alias) {
  var command = this;
  if (this.commands.length !== 0) {
    command = this.commands[this.commands.length - 1];
  }

  if (arguments.length === 0) return command._alias;

  if (alias === command._name) throw new Error('Command alias can\'t be the same as its name');

  command._alias = alias;
  return this;
};

/**
 * Set / get the command usage `str`.
 *
 * @param {String} str
 * @return {String|Command}
 * @api public
 */

Command.prototype.usage = function(str) {
  var args = this._args.map(function(arg) {
    return humanReadableArgName(arg);
  });

  var usage = '[options]' +
    (this.commands.length ? ' [command]' : '') +
    (this._args.length ? ' ' + args.join(' ') : '');

  if (arguments.length === 0) return this._usage || usage;
  this._usage = str;

  return this;
};

/**
 * Get or set the name of the command
 *
 * @param {String} str
 * @return {String|Command}
 * @api public
 */

Command.prototype.name = function(str) {
  if (arguments.length === 0) return this._name;
  this._name = str;
  return this;
};

/**
 * Return prepared commands.
 *
 * @return {Array}
 * @api private
 */

Command.prototype.prepareCommands = function() {
  return this.commands.filter(function(cmd) {
    return !cmd._noHelp;
  }).map(function(cmd) {
    var args = cmd._args.map(function(arg) {
      return humanReadableArgName(arg);
    }).join(' ');

    return [
      cmd._name +
        (cmd._alias ? '|' + cmd._alias : '') +
        (cmd.options.length ? ' [options]' : '') +
        (args ? ' ' + args : ''),
      cmd._description
    ];
  });
};

/**
 * Return the largest command length.
 *
 * @return {Number}
 * @api private
 */

Command.prototype.largestCommandLength = function() {
  var commands = this.prepareCommands();
  return commands.reduce(function(max, command) {
    return Math.max(max, command[0].length);
  }, 0);
};

/**
 * Return the largest option length.
 *
 * @return {Number}
 * @api private
 */

Command.prototype.largestOptionLength = function() {
  var options = [].slice.call(this.options);
  options.push({
    flags: '-h, --help'
  });
  return options.reduce(function(max, option) {
    return Math.max(max, option.flags.length);
  }, 0);
};

/**
 * Return the largest arg length.
 *
 * @return {Number}
 * @api private
 */

Command.prototype.largestArgLength = function() {
  return this._args.reduce(function(max, arg) {
    return Math.max(max, arg.name.length);
  }, 0);
};

/**
 * Return the pad width.
 *
 * @return {Number}
 * @api private
 */

Command.prototype.padWidth = function() {
  var width = this.largestOptionLength();
  if (this._argsDescription && this._args.length) {
    if (this.largestArgLength() > width) {
      width = this.largestArgLength();
    }
  }

  if (this.commands && this.commands.length) {
    if (this.largestCommandLength() > width) {
      width = this.largestCommandLength();
    }
  }

  return width;
};

/**
 * Return help for options.
 *
 * @return {String}
 * @api private
 */

Command.prototype.optionHelp = function() {
  var width = this.padWidth();

  // Append the help information
  return this.options.map(function(option) {
    return pad(option.flags, width) + '  ' + option.description +
      ((option.bool && option.defaultValue !== undefined) ? ' (default: ' + JSON.stringify(option.defaultValue) + ')' : '');
  }).concat([pad('-h, --help', width) + '  ' + 'output usage information'])
    .join('\n');
};

/**
 * Return command help documentation.
 *
 * @return {String}
 * @api private
 */

Command.prototype.commandHelp = function() {
  if (!this.commands.length) return '';

  var commands = this.prepareCommands();
  var width = this.padWidth();

  return [
    'Commands:',
    commands.map(function(cmd) {
      var desc = cmd[1] ? '  ' + cmd[1] : '';
      return (desc ? pad(cmd[0], width) : cmd[0]) + desc;
    }).join('\n').replace(/^/gm, '  '),
    ''
  ].join('\n');
};

/**
 * Return program help documentation.
 *
 * @return {String}
 * @api private
 */

Command.prototype.helpInformation = function() {
  var desc = [];
  if (this._description) {
    desc = [
      this._description,
      ''
    ];

    var argsDescription = this._argsDescription;
    if (argsDescription && this._args.length) {
      var width = this.padWidth();
      desc.push('Arguments:');
      desc.push('');
      this._args.forEach(function(arg) {
        desc.push('  ' + pad(arg.name, width) + '  ' + argsDescription[arg.name]);
      });
      desc.push('');
    }
  }

  var cmdName = this._name;
  if (this._alias) {
    cmdName = cmdName + '|' + this._alias;
  }
  var usage = [
    'Usage: ' + cmdName + ' ' + this.usage(),
    ''
  ];

  var cmds = [];
  var commandHelp = this.commandHelp();
  if (commandHelp) cmds = [commandHelp];

  var options = [
    'Options:',
    '' + this.optionHelp().replace(/^/gm, '  '),
    ''
  ];

  return usage
    .concat(desc)
    .concat(options)
    .concat(cmds)
    .join('\n');
};

/**
 * Output help information for this command
 *
 * @api public
 */

Command.prototype.outputHelp = function(cb) {
  if (!cb) {
    cb = function(passthru) {
      return passthru;
    };
  }
  process.stdout.write(cb(this.helpInformation()));
  this.emit('--help');
};

/**
 * Output help information and exit.
 *
 * @api public
 */

Command.prototype.help = function(cb) {
  this.outputHelp(cb);
  process.exit();
};

/**
 * Camel-case the given `flag`
 *
 * @param {String} flag
 * @return {String}
 * @api private
 */

function camelcase(flag) {
  return flag.split('-').reduce(function(str, word) {
    return str + word[0].toUpperCase() + word.slice(1);
  });
}

/**
 * Pad `str` to `width`.
 *
 * @param {String} str
 * @param {Number} width
 * @return {String}
 * @api private
 */

function pad(str, width) {
  var len = Math.max(0, width - str.length);
  return str + Array(len + 1).join(' ');
}

/**
 * Output help information if necessary
 *
 * @param {Command} command to output help for
 * @param {Array} array of options to search for -h or --help
 * @api private
 */

function outputHelpIfNecessary(cmd, options) {
  options = options || [];
  for (var i = 0; i < options.length; i++) {
    if (options[i] === '--help' || options[i] === '-h') {
      cmd.outputHelp();
      process.exit(0);
    }
  }
}

/**
 * Takes an argument an returns its human readable equivalent for help usage.
 *
 * @param {Object} arg
 * @return {String}
 * @api private
 */

function humanReadableArgName(arg) {
  var nameOutput = arg.name + (arg.variadic === true ? '...' : '');

  return arg.required
    ? '<' + nameOutput + '>'
    : '[' + nameOutput + ']';
}

// for versions before node v0.8 when there weren't `fs.existsSync`
function exists(file) {
  try {
    if (fs__default.statSync(file).isFile()) {
      return true;
    }
  } catch (e) {
    return false;
  }
}
});
var commander_1 = commander.Command;
var commander_2 = commander.Option;

commander
    .version("1.0.0")
    .option("-o --output <path>", "Output destination")
    .option("-i --input <path>", "Input destination")
    .parse(process.argv);
if (!commander.input) {
    console.error("ERROR: Must specify an input file");
    process.exitCode = 1;
}
else if (!commander.output) {
    console.error("ERROR: Must specify an output file");
    process.exitCode = 1;
}
else {
    var input = fs.readFileSync(path.resolve(commander.input)).toString();
    var output = path.resolve(commander.output);
    var tokens = lex(input);
    if (getErrors().length > 0) {
        throw new Error("Could not lex file:\n" +
            JSON.stringify(getErrorInfo(getErrors(), input), null, 2));
    }
    var ast = parse(tokens);
    if (getErrors().length > 0) {
        throw new Error("Could not parse file:\n" +
            JSON.stringify(getErrorInfo(getErrors(), input), null, 2));
    }
    fs.writeFileSync(output, render(ast));
}
