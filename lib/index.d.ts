import { File } from "gml";
export declare function render(file: File): string;
declare type PlaceholderMap = {
    [key: string]: string;
};
export declare function renderDefaultJSON(file: File): PlaceholderMap;
export {};
//# sourceMappingURL=index.d.ts.map