<< Number of teams in the game >>
NumTeams = 3

<< Number of rounds in the game >>
NumRounds = 4

<< Number of players in the game >>
NumRoles = 2

DefaultDocument = url("http://test.com/default.pdf")
DefaultVideo = video("http://test.com/default.mp4")
DefaultImage = image("http://test.com/default.jpg")
DefaultSound = sound("http://test.com/default.mp3")

DocumentList = list(DefaultDocument)

Feeling = set("HAPPY" "ANGRY" "NEUTRAL")

<<Teams are what define a player's goals and documents.>>
Team = {
    <<Team name (e.g. Department of State).>>
    name*: string

    <<High level description of the team and what its goals are.>>
    description: string

    <<Short sentence describing the team's main positive focus.>>
    want: string("Peace")

    <<Short sentence describing the team's main negative focus.>>
    fear: string("War")

    <<A small iconic image that identifies the team throughout the game UI.>>
    logo: DefaultImage

    <<A list of the pdfs the team members will review each round of the game>>
    documents: list(DocumentList NumRounds)
}

<<Factions are the groups that have a zero-sum interest in a decision (e.g. Corporations and Unions)>>
Faction = {
    <<Faction name>>
    name* : string

    <<Sound that plays whenever a decision is voted in favor of this faction>>
    soundId: DefaultSound
}

<<Decisions are what players are discussing each round.>>
Decision = {

    <<Decision name>>
    name : string

    << How each faction feels about this decision >>
    outcomes: {

        << How each faction feels about the decision passing the vote >>
        yes: map(ref(Faction) Feeling)

        << How each faction feels about the decision failing the vote >>
        no: map(ref(Faction) Feeling)
    }

    << High level description of the decision >>
    description : string("This is a decision")

    << The title for the decision that is going to be shown in the headers of the game UI >>
    title : string("Decision")

    << The subtitle for the decision that is going to be shown in the headers of the game UI >>
    subtitle : string("The choosening")

    << List of one to two videos that introduce the decision (which one is shown depends on the previous decision's outcome) >>
    introVideos:list(DefaultVideo [1 2])
}

<< Scenarios define all the data for a module or instance of the game. >>
Scenario = {

    << Scenario name >>
    name : string

    << A list of teams in the game >>
    teams : list(Team  NumTeams)

    << A list of factions in the game >>
    factions: list(Faction)

    << A list of the decisions for the rounds in the game >>
    decisions : list(Decision NumRounds)

    << List of videos corresponding to the possible outcomes of the game >>
    outcomeVideos : list(DefaultVideo 3)

    <<The order in which teams are introduced>>
    teamIntroOrder: list(ref(Team) NumTeams)

    <<The roles that will be assigned to players in the game>>
    roles : list(
        {
            <<The name of the role that will identify the player throughout the UI>>
            name : string

            <<The team the player belongs to>>
            team : ref(Team)

            <<A list of short sentences describing the players goals and interests>>
            interests : list(string)
        }
    NumRoles)
}