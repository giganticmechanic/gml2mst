NumTeams = 4
NumRounds = 3

PrimarySource = {
    description: string
    attribution: string
    image: image
}

Sources = list(PrimarySource [1 5])

Team = {
    name*: string
    description: string
    want: string
    fear: string
    documents: list(Sources NumRounds)
}

Decision = {
    name*: string
    description: string
    introVideos: list(video [1 2])
    side: set("Pro" "Con")
}

Scenario = {
        name*: string
        teams: list(Team NumTeams)
        decisions: list(Decision NumRounds)
        outcomeVideos: list(video 3)
        pages: {
            Welcome: {
                video: video
                title: string
                subtitle: string
            }
            Oath: {
                oathLines: list(string)
            }
            CrisisIntro: {
                video: video
                title: string
                subtitle: string
            }
            TeamsIntro: {
                title: string
                subtitle: string
                description: string
            }
        }
    }