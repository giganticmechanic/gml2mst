import typescript from "rollup-plugin-typescript2";
import resolve from "rollup-plugin-node-resolve";
import commonJS from "rollup-plugin-commonjs";
import pkg from "./package.json";

const cliConfig = {
    input: "src/cli/gml2mst.ts",
    output: [
        {
            file: "lib/cli/gml2mst.js",
            format: "cjs"
        }
    ],
    external: [
        "fs",
        "path",
        "events",
        "child_process",
        "util",
        "crypto"
        // ...Object.keys(pkg.dependencies || {}),
        //...Object.keys(pkg.peerDependencies || {}),
    ],
    plugins: [
        resolve({
            preferBuiltins: true
        }),
        commonJS({
            include: "node_modules/**"
        }),
        typescript({
            typescript: require("typescript"),
            tsconfig: "src/cli/tsconfig.json",
            cacheRoot: ".temp/rpt2",
            clean: true
        })
    ]
};

const libConfig = {
    input: "src/library/index.ts",
    output: [
        {
            file: pkg.main,
            format: "cjs"
        },
        {
            file: pkg.module,
            format: "es"
        }
    ],
    external: [
        "fs",
        "path",
        "events",
        "child_process",
        "util",
        "crypto",
        "gml"
        // ...Object.keys(pkg.dependencies || {}),
        //...Object.keys(pkg.peerDependencies || {}),
    ],
    plugins: [
        resolve({
            preferBuiltins: true
        }),
        commonJS({
            include: "node_modules/**"
        }),
        typescript({
            typescript: require("typescript"),
            tsconfig: "src/library/tsconfig.json",
            cacheRoot: ".temp/rpt2",
            clean: true
        })
    ]
};

export default [cliConfig, libConfig];
